package com.waveon.gatepassway.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.waveon.gatepassway.GatePassWay;
import com.waveon.gatepassway.R;
import com.waveon.gatepassway.usbserial.CustomProber;
import com.waveon.gatepassway.utils.HexDump;

import java.io.IOException;
import java.util.concurrent.Executors;

public class FloatSerialService extends Service implements SerialInputOutputManager.Listener{
    private static final String TAG = "ScreenCaptureService";

    private WindowManager mWindowManager;
    private View mFloatingWidget;

    private int portNum, baudRate;
    private boolean withIoManager;

    private enum UsbPermission { Unknown, Requested, Granted, Denied };

    private SerialInputOutputManager usbIoManager;
    private UsbSerialPort usbSerialPort;
    private UsbPermission usbPermission = UsbPermission.Unknown;
    private boolean connected = false;

    private Handler mainLooper;
    private static final int WRITE_WAIT_MILLIS = 2000;
    private static final int READ_WAIT_MILLIS = 2000;

    public FloatSerialService() {
    }


    private void showShortMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private static final String ACTION_USB_PERMISSION =
            "com.android.example.USB_PERMISSION";


    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    usbPermission = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                            ? UsbPermission.Granted : UsbPermission.Denied;
                    connect();
                }
            }
        }
    };

    private final BroadcastReceiver connectReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (GatePassWay.BROAD_CAST_REQUEST_ARDUINO_CON.equals(action)) {
                synchronized (this) {
                    usbPermission = UsbPermission.Unknown;
                    if(connected)
                        disconnect();
                    connect();
                }
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        portNum = 0;
        baudRate = 9600;
        withIoManager = true;

        mainLooper = new Handler(Looper.getMainLooper());

        int LAYOUT_FLAG;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }else{
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }


        mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.layout_floating_camera_widget, null);

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = metrics.widthPixels;

        params.gravity = Gravity.TOP | Gravity.RIGHT;
        params.x = screenWidth - 50;
        params.y = 80;

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        IntentFilter filter = new IntentFilter(GatePassWay.BROAD_CAST_REQUEST_ARDUINO_CON);
        registerReceiver(connectReceiver, filter);

        IntentFilter serialSendFilter = new IntentFilter();
        serialSendFilter.addAction(GatePassWay.BROAD_CAST_SERIAL_SEND_MESSAGE);
        registerReceiver(serialSendReceiver, serialSendFilter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(usbReceiver);
            unregisterReceiver(connectReceiver);
            unregisterReceiver(serialSendReceiver);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onNewData(final byte[] data) {
        mainLooper.post(new Runnable() {
            @Override
            public void run() {
                receive(data);
            }
        });
    }

    @Override
    public void onRunError(Exception e) {
        mainLooper.post(new Runnable() {
            @Override
            public void run() {
                usbPermission = UsbPermission.Unknown;
                disconnect();
                sendStatusBroadcastMsg();
            }
        });
    }

    /*
     * Serial + UI
     */
    private void connect() {
        UsbDevice device = null;
        UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        for(UsbDevice v : usbManager.getDeviceList().values()) {
            if (v.getDeviceId() == GatePassWay.deviceID)
                device = v;
        }
        if(device == null) {
            status("connection failed: device not found by Service");
            return;
        }
        UsbSerialDriver driver = UsbSerialProber.getDefaultProber().probeDevice(device);
        if(driver == null) {
            driver = CustomProber.getCustomProber().probeDevice(device);
        }
        if(driver == null) {
            status("연결 실패: no driver for device");
            return;
        }
        if(driver.getPorts().size() < portNum) {
            status("연결 실패: not enough ports at device");
            return;
        }
        usbSerialPort = driver.getPorts().get(portNum);
        UsbDeviceConnection usbConnection = usbManager.openDevice(driver.getDevice());


        if(usbConnection == null && usbPermission == UsbPermission.Unknown && !usbManager.hasPermission(driver.getDevice())) {
            usbPermission = UsbPermission.Requested;
            PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(usbReceiver, filter);
            usbManager.requestPermission(driver.getDevice(), usbPermissionIntent);
            return;
        }

        if(usbConnection == null) {
            if (!usbManager.hasPermission(driver.getDevice())) {
                status("연결 실패: permission denied");
            }
            else
                status("연결 실패: open failed");
            return;
        }

        try {
            usbSerialPort.open(usbConnection);
            usbSerialPort.setParameters(baudRate, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
            if(withIoManager) {
                usbIoManager = new SerialInputOutputManager(usbSerialPort, this);
                Executors.newSingleThreadExecutor().submit(usbIoManager);
            }
            //status("연결 완료");
            connected = true;
        } catch (Exception e) {
            //status("연결 실패: " + e.getMessage());
            disconnect();
        }

        sendStatusBroadcastMsg();
    }

    private void disconnect() {
        connected = false;
        if(usbIoManager != null)
            usbIoManager.stop();
        usbIoManager = null;
        try {
            if(usbSerialPort != null )
                usbSerialPort.close();
        } catch (IOException ignored) {}
        usbSerialPort = null;

        sendStatusBroadcastMsg();
    }

    void sendStatusBroadcastMsg(){
        Intent intent = new Intent(GatePassWay.BROAD_CAST_ARDUINO_STATUS_MESSAGE);
        intent.putExtra("status", connected);
        getApplication().sendBroadcast(intent);
    }

    void status(String str) {
        SpannableStringBuilder spn = new SpannableStringBuilder(str+'\n');
        spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorStatusText)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        showShortMsg(str);
    }

    private void receive(byte[] data) {
        SpannableStringBuilder spn = new SpannableStringBuilder();
        spn.append("receive " + data.length + " bytes\n");
        if(data.length > 0)
            spn.append(HexDump.dumpHexString(data)+"\n");


        int val = data[0] - '0';

        if(val == 1) {
            getApplication().sendBroadcast(new Intent(GatePassWay.BROAD_CAST_ARDUINO_MESSAGE));
            //getActivity().sendBroadcast(new Intent(TempChecker.BROAD_CAST_REQEUST_CLICK));
        }
    }

    private final BroadcastReceiver serialSendReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (GatePassWay.BROAD_CAST_SERIAL_SEND_MESSAGE.equals(action)) {
                synchronized (this) {
                    try {
                        send("on\r\n");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        }
    };

    public void send(String str) {
        if(!connected) {
            //Toast.makeText(getActivity(), "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            byte[] data = (str).getBytes();
            usbSerialPort.write(data, WRITE_WAIT_MILLIS);
        } catch (Exception e) {
            onRunError(e);
        }
    }
}
