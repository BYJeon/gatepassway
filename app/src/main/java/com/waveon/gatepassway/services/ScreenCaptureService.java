package com.waveon.gatepassway.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;


import com.googlecode.tesseract.android.TessBaseAPI;
import com.waveon.gatepassway.GatePassWay;
import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.DetectorActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

public class ScreenCaptureService extends Service {
    private MediaRecorder mediaRecorder;
    private VirtualDisplay virtualDisplay;
    private boolean running;
    private int width = 720;
    private int height = 1080;
    private int dpi;
    private ImageReader mImageReader;
    private MediaProjection mediaProjection;

    private static final String CHANNEL_ID = "screen_capture";
    private static final String CHANNEL_NAME = "Screen_Capture";

//    private TessBaseAPI tess;
//    private String mDataPath = "";

    TimerTask timerTask;
    Timer timer = new Timer();

    private boolean mIsProcessing = false;

    @Override
    public IBinder onBind(Intent intent) {
        return new RecordBinder();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        running = false;
        mediaRecorder = new MediaRecorder();

//        mDataPath = getFilesDir() + "/tesseract/";
//        checkFile(new File(mDataPath + "tessdata/"), "kor");
//
//        String lang = "kor";
//        tess = new TessBaseAPI();
//        tess.init(mDataPath, lang);


        startTimerTask();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimerTask();
    }


    public void setMediaProject(MediaProjection project) {
        mediaProjection = project;
    }


    public boolean isRunning() {
        return running;
    }


    public void setConfig(int width, int height, int dpi) {
        this.width = width;
        this.height = height;
        this.dpi = dpi;
    }


    /**
     * 开始录屏
     *
     * @return true
     */
    public boolean startRecord() {
        if (mediaProjection == null || running) {
            return false;
        }
        initRecorder();
        createVirtualDisplay();
        mediaRecorder.start();
        running = true;
        return true;
    }


    /**
     * 结束录屏
     *
     * @return true
     */
    public boolean stopRecord() {
        if (!running) {
            return false;
        }
        running = false;
        mediaRecorder.stop();
        mediaRecorder.reset();
        virtualDisplay.release();
        mediaProjection.stop();

        return true;
    }


    public void setMediaProjection(MediaProjection mediaProjection) {
        this.mediaProjection = mediaProjection;
    }


    /**
     * 初始化ImageRead参数
     */
    public void initImageReader() {
        if (mImageReader == null) {
            int maxImages = 10;
            mImageReader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, maxImages);
            createImageVirtualDisplay();
        }
    }


    /**
     * 创建一个录屏 Virtual
     */

    private void createVirtualDisplay() {
        virtualDisplay = mediaProjection
                .createVirtualDisplay("mediaprojection", width, height, dpi, DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mediaRecorder
                        .getSurface(), null, null);
    }


    /**
     * 创建一个ImageReader Virtual
     */
    private void createImageVirtualDisplay() {
        virtualDisplay = mediaProjection
                .createVirtualDisplay("mediaprojection", width, height, dpi,
                        DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mImageReader
                        .getSurface(), null, null);
    }


    /**
     * 初始化保存屏幕录像的参数
     */
    private void initRecorder() {
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(
                getSavePath() + System.currentTimeMillis() + ".mp4");
        mediaRecorder.setVideoSize(width, height);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setVideoEncodingBitRate(5 * 1024 * 1024);
        mediaRecorder.setVideoFrameRate(30);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取一个保存屏幕录像的路径
     *
     * @return path
     */
    public String getSavePath() {
        if (Environment.getExternalStorageState()
                       .equals(Environment.MEDIA_MOUNTED)) {
            String rootDir = Environment.getExternalStorageDirectory()
                                        .getAbsolutePath() + "/" +
                    "ScreenRecord" + "/";

            File file = new File(rootDir);
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    return null;
                }
            }
            return rootDir;
        } else {
            return null;
        }
    }


    /**
     * 请求完权限后马上获取有可能为null，可以通过判断is null来重复获取。
     */
    public Bitmap getBitmap() {
        Bitmap bitmap = cutoutFrame();
//        if (bitmap == null) {
//            getBitmap();
//        }
        return bitmap;
    }


    /**
     * 通过底层来获取下一帧的图像
     *
     * @return bitmap
     */
    public Bitmap cutoutFrame() {
        if(mImageReader == null) return null;
        Image image = mImageReader.acquireLatestImage();
        if (image == null) {
            return null;
        }

        int width = image.getWidth();
        int height = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        Bitmap bitmap = Bitmap.createBitmap(width +
                rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        image.close();
        return Bitmap.createBitmap(bitmap, 0, 0, width, height);
    }


    public class RecordBinder extends Binder {
        public ScreenCaptureService getRecordService() {
            return ScreenCaptureService.this;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startForeground() {
        NotificationChannel chan = new NotificationChannel(CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_NONE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        final int notificationId = (int) System.currentTimeMillis();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_screen_share_white_24dp)
                .setContentTitle("ScreenCapturerService is running in the foreground")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(notificationId, notification);
    }


    public void endForeground() {
        stopForeground(true);
    }

    private void startTimerTask()
    {
        timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                Bitmap bitmap = getBitmap();
                if (bitmap == null) return;
                GatePassWay.captureBmp = cropBitmap(bitmap, bitmap.getWidth() , 200);
                sendBroadcast(new Intent(GatePassWay.BROAD_CAST_CAPTURE_UPDATE_MESSAGE));
                Log.d("Timer", "Service");
                //processImage(GatePassWay.captureBmp);
            }
        };
        timer.schedule(timerTask,0 ,800);
    }

    public Bitmap cropBitmap(Bitmap bitmap, int width, int height) {
        int originWidth = bitmap.getWidth();
        int originHeight = bitmap.getHeight();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenHeight = metrics.heightPixels;

        // 이미지를 crop 할 좌상단 좌표
        int x = 350;
        int y = 100;
        Bitmap cropedBitmap = Bitmap.createBitmap(bitmap, x, y, width - 700, height);


        return cropedBitmap;
    }

    private void stopTimerTask()
    {
        if(timerTask != null)
        {
            //Log.d("Timer", "stopTimerTask");
            timerTask.cancel();
            timerTask = null;
        }

    }

//    private void checkFile(File dir, String Language){
//        //디렉토리가 없으면 디렉토리를 만들고 그후에 파일을 카피
//        if (!dir.exists() && dir.mkdirs()) {
//            copyFiles(Language);
//        }
//        //디렉토리가 있지만 파일이 없으면 파일카피 진행
//        if (dir.exists()) {
//            String datafilepath = mDataPath + "tessdata/" + Language + ".traineddata";
//            File datafile = new File(datafilepath);
//            if (!datafile.exists()) {
//                copyFiles(Language);
//            }
//        }
//    }
//
//    //copy file to device
//    private void copyFiles(String Language) {
//        try {
//            String filepath = mDataPath + "/tessdata/" + Language + ".traineddata";
//            AssetManager assetManager = getAssets();
//            InputStream instream = assetManager.open("tessdata/"+Language+".traineddata");
//            OutputStream outstream = new FileOutputStream(filepath);
//            byte[] buffer = new byte[1024];
//            int read;
//            while ((read = instream.read(buffer)) != -1) {
//                outstream.write(buffer, 0, read);
//            }
//            outstream.flush();
//            outstream.close();
//            instream.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void processImage(final Bitmap bitmap) {
//        if(bitmap == null) return;
//
//        new Handler(getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                tess.setImage(bitmap);
//                final String OCRResult = tess.getUTF8Text();
//                Log.d("Timer OCR", OCRResult == null ? "Empty" : OCRResult);
//
//                if (OCRResult.contains("인중되었습니다") && !mIsProcessing || OCRResult.contains("인증되었습니다") && !mIsProcessing ||
//                        OCRResult.contains("등록된") && !mIsProcessing || OCRResult.contains("드록되") && !mIsProcessing || OCRResult.contains("이미") && !mIsProcessing ||
//                        OCRResult.contains("드록된") && !mIsProcessing ) {
//                    mIsProcessing = true;
//                    Intent intent = new Intent(getBaseContext(), DetectorActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
//                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            mIsProcessing = false;
//                        }
//                    }, 3000);
//                }
//            }
//        });
//
//
//        //Log.d("OCR", OCRResult == null ? "Empty" : OCRResult);
//
//    }
}
