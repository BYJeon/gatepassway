package com.waveon.gatepassway.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.usb.UsbDevice;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.jiangdg.usbcamera.UVCCameraHelper;
import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.common.AbstractUVCCameraHandler;
import com.serenegiant.usb.widget.CameraViewInterface;
import com.waveon.gatepassway.GatePassWay;
import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.DetectorActivity;
import com.waveon.gatepassway.usbserial.CustomProber;


public class FloatCameraService extends Service implements CameraDialog.CameraDialogParent, CameraViewInterface.Callback{
    private static final String TAG = "ScreenCaptureService";

    private WindowManager mWindowManager;
    private View mFloatingWidget;


    private UVCCameraHelper mCameraHelper = null;
    private CameraViewInterface mUVCCameraView = null;

    private boolean isPreview;
    private WindowManager.LayoutParams params = null;
    private Handler mainLooper;
    private enum UsbPermission { Unknown, Requested, Granted, Denied };

    private SerialInputOutputManager usbIoManager;
    private UsbSerialPort usbSerialPort;
    private UsbPermission usbPermission = UsbPermission.Unknown;

    private boolean connected = false;
    private int mDeviceID;

    private int portNum, baudRate;
    private boolean withIoManager;

    public FloatCameraService() {
    }

    private void showShortMsg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }


    private UVCCameraHelper.OnMyDevConnectListener listener = new UVCCameraHelper.OnMyDevConnectListener() {

        @Override
        public void onAttachDev(UsbDevice device) {
            //showShortMsg(device.getProductId() + " AttachDev Main");
            UsbSerialProber usbDefaultProber = UsbSerialProber.getDefaultProber();
            UsbSerialProber usbCustomProber = CustomProber.getCustomProber();

            //showShortMsg( Integer.toString(device.getProductId()) + " : Service");


            if (device.getProductId() == 29987) {
                mDeviceID = device.getDeviceId();
                GatePassWay.deviceID = mDeviceID;
                sendBroadcast(new Intent(GatePassWay.BROAD_CAST_REQUEST_ARDUINO_CON));
                //connect();
            }

            if (device.getProductId() == 24577) {
                mDeviceID = device.getDeviceId();
                GatePassWay.deviceID = mDeviceID;
                sendBroadcast(new Intent(GatePassWay.BROAD_CAST_REQUEST_ARDUINO_CON));
                //connect();
            }

        }

        @Override
        public void onDettachDev(UsbDevice device) {
            //특정 카메라 ID
        }

        @Override
        public void onConnectDev(UsbDevice device, boolean isConnected) {
            if (!isConnected) {
                showShortMsg("fail to connect,please check resolution params");
                isPreview = false;
            } else {
                isPreview = true;
            }
        }

        @Override
        public void onDisConnectDev(UsbDevice device) {

        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        portNum = 0;
        baudRate = 9600;
        withIoManager = true;

        mainLooper = new Handler(Looper.getMainLooper());

        int LAYOUT_FLAG;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }else{
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.layout_floating_camera_widget, null);
        //mFloatingWidget.findViewById(R.id.collapse_view).setVisibility(View.INVISIBLE);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 50;
        params.y = 80;

        params.width = 300;
        params.height = 300;



        mUVCCameraView = (CameraViewInterface) mFloatingWidget.findViewById(R.id.camera_view);
        mUVCCameraView.setCallback(this);
        mCameraHelper = UVCCameraHelper.getInstance(this);
        mCameraHelper.setDefaultFrameFormat(UVCCameraHelper.FRAME_FORMAT_MJPEG);
        mCameraHelper.initUSBMonitor(DetectorActivity.getActivity(), mUVCCameraView, listener);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mCameraHelper != null) {
            mCameraHelper.unregisterUSB();
        }

        unregisterReceiver(serviceStopReceiver);

        // step.4 release uvc camera resources
        if (mCameraHelper != null) {
            mCameraHelper.release();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mCameraHelper != null) {
            mCameraHelper.registerUSB();
        }

        if(mWindowManager != null) {
            try {
                mWindowManager.removeView(mFloatingWidget);
            }catch (Exception e){
                e.printStackTrace();
            }

            params.gravity = Gravity.TOP | Gravity.LEFT;
            params.x = 50;
            params.y = 80;

            params.width = 200;
            params.height = 200;
            mFloatingWidget.setVisibility(View.VISIBLE);
            mWindowManager.addView(mFloatingWidget, params);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void sendStatusBroadcastMsg(boolean status, String filter){
        Intent intent = new Intent(filter);
        intent.putExtra("status", status);
        sendBroadcast(intent);
    }

    private final BroadcastReceiver serviceStopReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
//            if (TempChecker.BROAD_CAST_REQUEST_SERVICE_STOP.equals(action)) {
//                synchronized (this) {
//                    try {
//                        mWindowManager.removeView(mFloatingWidget);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                    params.width = 2;
//                    params.height = 2;
//                    params.x = 0;
//                    params.y = 0;
//                    mWindowManager.addView(mFloatingWidget, params);
//
//                }
//            }
        }
    };


    @Override
    public USBMonitor getUSBMonitor() {
        return mCameraHelper.getUSBMonitor();
    }

    @Override
    public void onDialogResult(boolean canceled) {

    }

    @Override
    public void onSurfaceCreated(CameraViewInterface view, Surface surface) {
        if (!isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.setOnPreviewFrameListener(new AbstractUVCCameraHandler.OnPreViewResultListener() {
                @Override
                public void onPreviewResult(byte[] nv21Yuv) {
                    Log.d(TAG, "onPreviewResult: "+nv21Yuv.length);

                    showShortMsg("YUV");

//                int [] rgbBytes = new int[previewWidth * previewHeight];
//                ImageUtils.convertYUV420SPToARGB8888(nv21Yuv, 720, 1080, rgbBytes);
//                Bitmap mViewBitmap = Bitmap.createBitmap(720, 1080, Bitmap.Config.ARGB_8888);
//                mViewBitmap.setPixels(rgbBytes, 0, 720, 0, 0, 720, 1080);
//                mBinding.ivYuv.setImageBitmap(mViewBitmap);
                }
            });

            mCameraHelper.startPreview(mUVCCameraView);

            isPreview = true;
        }
    }

    @Override
    public void onSurfaceChanged(CameraViewInterface view, Surface surface, int width, int height) {

    }

    @Override
    public void onSurfaceDestroy(CameraViewInterface view, Surface surface) {
        if (isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.stopPreview();
            isPreview = false;

            mCameraHelper.setOnPreviewFrameListener(null);
        }
    }
}
