package com.waveon.gatepassway.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Preferences {
    private static final String TAG = "Preferences";
    private static final String NAME = "tempchecker";

    private static Context mApplicationContext = null;

    public static void init(Context context) {
        mApplicationContext = context;
    }

    private static final String KEY_TEMP_UPPER = "key_temp_upper";
    private static final String KEY_TEMP_UNDER = "key_temp_under";
    private static final String KEY_DETECT_NOSE = "key_detect_nose";

    private static SharedPreferences getPreferences() {
        if (mApplicationContext == null) {
            Log.e(TAG, "error preferences not init");
            return null;
        }

        return mApplicationContext.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
    }

    /* preferences에 string을 기록한다 */
    private static void putString(String key, String value) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init putString key : " + key + " value : " + value);
            return;
        }

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(key, value);
        editor.apply();
    }

    private static String getString(String key) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return "";
        }

        return pref.getString(key, "");
    }

    private static String getString(String key, String defaults) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return "";
        }

        return pref.getString(key, defaults);
    }


    /* preferences에 Integer를 기록한다 */
    private static void putInteger(String key, int value) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init putString key : " + key + " value : " + value);
            return;
        }

        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(key, value);
        editor.apply();
    }


    private static int getInteger(String key, int def) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return 0;
        }

        return pref.getInt(key, def);
    }

    private static boolean getBoolean(String key) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return false;
        }

        return pref.getBoolean(key, true);
    }

    private static void putBoolean(String key, boolean value) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init putString key : " + key + " value : " + value);
            return;
        }

        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void setUppderTemp(int value) {
        putInteger(KEY_TEMP_UPPER, value);
    }
    public static int getUpperTemp() {
        return getInteger(KEY_TEMP_UPPER, 376);
    }

    public static void setUnderTemp(int value) {
        putInteger(KEY_TEMP_UNDER, value);
    }
    public static int getUnderTemp() {
        return getInteger(KEY_TEMP_UNDER, 359);
    }

    public static void setDetectNose(boolean value) {
        putBoolean(KEY_DETECT_NOSE, value);
    }
    public static boolean getDetectNose() {
        return getBoolean(KEY_DETECT_NOSE);
    }
}
