package com.waveon.gatepassway.usbserial;

import android.hardware.usb.UsbDevice;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

public class DeviceListItem {
    UsbDevice device;
    int port;
    UsbSerialDriver driver;

    public DeviceListItem(UsbDevice device, int port, UsbSerialDriver driver) {
        this.device = device;
        this.port = port;
        this.driver = driver;
    }

    public UsbSerialDriver getUsbSerialDriver(){return this.driver;}
    public int getPort(){return this.port;}
    public UsbDevice getUsbDevice(){return this.device;}
}
