package com.waveon.gatepassway;

import android.graphics.Bitmap;

public class GatePassWay {
    public static final String BROAD_CAST_ARDUINO_MESSAGE = "kr.co.waveon.arduino_message";
    public static final String BROAD_CAST_ARDUINO_STATUS_MESSAGE = "kr.co.waveon.arduino_status_message";
    public static final String BROAD_CAST_REQUEST_ARDUINO_CON = "com.waveon.floatingtest.request_arduino_con";
    public static final String BROAD_CAST_CAPTURE_UPDATE_MESSAGE = "kr.co.damo.capture_update_message";
    public static final String BROAD_CAST_SERIAL_SEND_MESSAGE = "kr.co.damo.serial_send_message";

    public static int deviceID;
    public static Bitmap captureBmp;
}
