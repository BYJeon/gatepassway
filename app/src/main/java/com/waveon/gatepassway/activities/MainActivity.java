package com.waveon.gatepassway.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.base.BindActivity;
import com.waveon.gatepassway.databinding.ActivityMainBinding;

public class MainActivity extends BindActivity<ActivityMainBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void setUIEventListener() {
        mBinding.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = getPackageManager().getLaunchIntentForPackage("com.jinsit.safepass");
                it.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(it);
            }
        });
    }
}