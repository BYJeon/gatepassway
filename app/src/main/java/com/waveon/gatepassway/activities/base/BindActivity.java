package com.waveon.gatepassway.activities.base;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.waveon.gatepassway.R;
import com.waveon.gatepassway.utils.CommonUtils;


public abstract class BindActivity<B extends ViewDataBinding> extends AppCompatActivity {
    protected B mBinding;

    protected abstract int getLayoutId();

    /**
     * 화면 초기화
     * onCreate() 함수가 호출된 후 Call
     */
    protected abstract void initView();

    /**
     * UI Component EventListener
     */
    protected abstract void setUIEventListener();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mBinding = DataBindingUtil.setContentView(this, getLayoutId());
        initView();
        setUIEventListener();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.bg_color));
            View view = getWindow().getDecorView();
            view.setSystemUiVisibility(view.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    protected void onPause() {

        try {
            View view = getCurrentFocus();

            if (view != null) {
                if (view instanceof EditText) {
                    CommonUtils.hideKeypad(this, view);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    protected void showShortMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
