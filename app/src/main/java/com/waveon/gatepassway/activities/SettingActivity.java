package com.waveon.gatepassway.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.base.BindActivity;
import com.waveon.gatepassway.databinding.ActivitySettingBinding;
import com.waveon.gatepassway.utils.Preferences;


public class SettingActivity extends BindActivity<ActivitySettingBinding> {
    private static final String TAG = "Settingctivity";
    private final int tmpGab = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {

        int upperTmp = Preferences.getUpperTemp();
        mBinding.etUpper.setText(Integer.toString(upperTmp/10) + "." + Integer.toString(upperTmp%10));

        int underTmp = Preferences.getUnderTemp();
        mBinding.etUnder.setText(Integer.toString(underTmp/10) + "." + Integer.toString(underTmp%10));

        boolean detectNose = Preferences.getDetectNose();
        mBinding.swNose.setSelected(detectNose);
        setUIEventListener();
    }

    @Override
    protected void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mBinding.layoutSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !mBinding.swNose.isSelected();
                mBinding.swNose.setSelected(isSelected);
                Preferences.setDetectNose(isSelected);
            }
        });

        mBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tmpUpper = mBinding.etUpper.getText().toString();
                int tmpUpperFloat = (int)(Float.parseFloat(tmpUpper) * 10);

                String tmpUnder = mBinding.etUnder.getText().toString();
                int tmpUnderFloat = (int)(Float.parseFloat(tmpUnder) * 10);

                Preferences.setUppderTemp(tmpUpperFloat);
                Preferences.setUnderTemp(tmpUnderFloat);

                Toast toast = Toast.makeText(SettingActivity.this, "저장 완료", Toast.LENGTH_SHORT);
                ((TextView)((LinearLayout)toast.getView()).getChildAt(0))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });


        mBinding.ivUpperAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmp = mBinding.etUpper.getText().toString();
                int tmpFloat = (int)(Float.parseFloat(tmp) * 10);
                tmpFloat += tmpGab;
                mBinding.etUpper.setText( Integer.toString(tmpFloat/10) + "." + Integer.toString(tmpFloat%10));
            }
        });

        mBinding.ivUpperMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmp = mBinding.etUpper.getText().toString();
                int tmpFloat = (int)(Float.parseFloat(tmp) * 10);
                tmpFloat -= tmpGab;
                mBinding.etUpper.setText( Integer.toString(tmpFloat/10) + "." + Integer.toString(tmpFloat%10));
            }
        });

        mBinding.ivUnderAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmp = mBinding.etUnder.getText().toString();
                int tmpFloat = (int)(Float.parseFloat(tmp) * 10);
                tmpFloat += tmpGab;
                mBinding.etUnder.setText( Integer.toString(tmpFloat/10) + "." + Integer.toString(tmpFloat%10));
            }
        });

        mBinding.ivUnderMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmp = mBinding.etUnder.getText().toString();
                int tmpFloat = (int)(Float.parseFloat(tmp) * 10);
                tmpFloat -= tmpGab;
                mBinding.etUnder.setText( Integer.toString(tmpFloat/10) + "." + Integer.toString(tmpFloat%10));
            }
        });
    }
}