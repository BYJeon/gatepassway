package com.waveon.gatepassway.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.hardware.camera2.CameraCharacteristics;
import android.media.ImageReader.OnImageAvailableListener;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.waveon.gatepassway.GatePassWay;
import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.base.CameraActivity;
import com.waveon.gatepassway.facedetector.customview.OverlayView;
import com.waveon.gatepassway.facedetector.env.BorderedText;
import com.waveon.gatepassway.facedetector.env.ImageUtils;
import com.waveon.gatepassway.facedetector.env.Logger;
import com.waveon.gatepassway.facedetector.tflite.Classifier;
import com.waveon.gatepassway.facedetector.tflite.TFLiteObjectDetectionAPIModel;
import com.waveon.gatepassway.facedetector.tracking.MultiBoxTracker;
import com.waveon.gatepassway.services.FloatSerialService;
import com.waveon.gatepassway.services.ScreenCaptureService;
import com.waveon.gatepassway.utils.Preferences;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class DetectorActivity extends CameraActivity implements OnImageAvailableListener {
    private static final Logger LOGGER = new Logger();
    private static final int APP_PERMISSION_REQUEST = 102;

    // Configuration values for the prepackaged SSD model.
    //private static final int TF_OD_API_INPUT_SIZE = 300;
    //private static final boolean TF_OD_API_IS_QUANTIZED = true;
    //private static final String TF_OD_API_MODEL_FILE = "detect.tflite";
    //private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/labelmap.txt";

    //private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);

    // Face Mask
    private static final int TF_OD_API_INPUT_SIZE = 224;
    private static final boolean TF_OD_API_IS_QUANTIZED = false;
    private static final String TF_OD_API_MODEL_FILE = "model.tflite";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/labels.txt";

    private static final DetectorMode MODE = DetectorMode.TF_OD_API;
    // Minimum detection confidence to track a detection.
    private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.5f;
    private static final boolean MAINTAIN_ASPECT = false;

    private static final Size DESIRED_PREVIEW_SIZE = new Size(800, 600);
    //private static final int CROP_SIZE = 320;
    //private static final Size CROP_SIZE = new Size(320, 320);



    private static final boolean SAVE_PREVIEW_BITMAP = false;
    private static final float TEXT_SIZE_DIP = 10;
    OverlayView trackingOverlay;
    private Integer sensorOrientation;

    private Classifier detector;

    private long lastProcessingTimeMs;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Bitmap cropCopyBitmap = null;

    private boolean computingDetection = false;

    private long timestamp = 0;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;

    private MultiBoxTracker tracker;

    private BorderedText borderedText;

    // Face detector
    private FaceDetector faceDetector;

    // here the preview image is drawn in portrait way
    private Bitmap portraitBmp = null;
    // here the face is cropped and drawn
    private Bitmap faceBmp = null;
    private Mat faceMat;
    private boolean mIsNoe = false;

    private File cascFile;
    private CascadeClassifier faceDetectorCV;
    private static Activity mCurrentActivity = null;
    private boolean mIsNose = false;

    private MediaProjectionManager projectionManager;
    private MediaProjection mediaProjection;
    private ScreenCaptureService recordService;
    private static int RECORD_REQUEST_CODE = 5;

    private TessBaseAPI tess;
    private String mDataPath = "";
    TimerTask timerTask;
    Timer timer = new Timer();
    private boolean mIsProcessing = false;
    private int mTimeCount = 0;
    private final int mMaxTimeCount = 4;
    private TextToSpeech mTTS;
    private boolean mIsNewTask = true;
    private final int mMaskResltFrame = 3;

    private ArrayList<Integer> mMaskResultArr = new ArrayList<>();

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            ScreenCaptureService.RecordBinder binder = (ScreenCaptureService.RecordBinder) service;
            recordService = binder.getRecordService();
            recordService.setConfig(metrics.widthPixels, metrics.heightPixels, metrics.densityDpi);
            recordService.startForeground();

            Intent captureIntent = projectionManager.createScreenCaptureIntent();
            startActivityForResult(captureIntent, RECORD_REQUEST_CODE);
        }


        @Override
        public void onServiceDisconnected(ComponentName arg0) {}
    };

    private BaseLoaderCallback baseCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case LoaderCallbackInterface.SUCCESS:{
                    InputStream is = getResources().openRawResource(R.raw.haarcascades_haarcascade_mcs_nose);
                    File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                    cascFile = new File(cascadeDir, "haarcascades_haarcascade_mcs_nose.xml");

                    try {
                        FileOutputStream fos = new FileOutputStream(cascFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ( (bytesRead = is.read(buffer)) != -1) {
                            fos.write(buffer, 0, bytesRead);
                        }

                        is.close();
                        fos.close();
                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }

                    faceDetectorCV = new CascadeClassifier(cascFile.getAbsolutePath());
                    if(faceDetectorCV.empty()){
                        faceDetectorCV = null;
                    }else
                        cascadeDir.delete();

                    faceMat = new Mat();
                }
                break;

                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
            super.onManagerConnected(status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCurrentActivity = this;

        if(!OpenCVLoader.initDebug()){
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, baseCallback);
        }else{
            baseCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        // Real-time contour detection of multiple faces
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        faceDetector = detector;
        //checkWritePermission();

        if(!OpenCVLoader.initDebug()){
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, baseCallback);
        }else{
            baseCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        projectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent i = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(i, APP_PERMISSION_REQUEST);
        }

        Intent intent = new Intent(this, ScreenCaptureService.class);
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);

        //startService(new Intent(DetectorActivity.this, FloatWidgetService.class));

        mDataPath = getFilesDir() + "/tesseract/";
        checkFile(new File(mDataPath + "tessdata/"), "kor");

        String lang = "kor";
        tess = new TessBaseAPI();
        tess.init(mDataPath, lang);
        tess.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_COLUMN);

        initTTS();
    }

    public static Activity getActivity(){
        return mCurrentActivity;
    }

    @Override
    public synchronized void onStart() {
        super.onStart();

        startService(new Intent(DetectorActivity.this, FloatSerialService.class));
        //startService(new Intent(DetectorActivity.this, FloatCameraService.class));

        initBroadCastReceiver();
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();

        stopTimerTask();
        unregisterReceiver(mArduninoReceiver);
        unregisterReceiver(mCaptureReceiver);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {
        final float textSizePx =
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.MONOSPACE);

        tracker = new MultiBoxTracker(this);


        try {
            detector =
                    TFLiteObjectDetectionAPIModel.create(
                            getAssets(),
                            TF_OD_API_MODEL_FILE,
                            TF_OD_API_LABELS_FILE,
                            TF_OD_API_INPUT_SIZE,
                            TF_OD_API_IS_QUANTIZED);
            //cropSize = TF_OD_API_INPUT_SIZE;
        } catch (final IOException e) {
            e.printStackTrace();
            LOGGER.e(e, "Exception initializing classifier!");
            Toast toast =
                    Toast.makeText(
                            getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }

        previewWidth = size.getWidth();
        previewHeight = size.getHeight();

        int screenOrientation = getScreenOrientation();
        sensorOrientation = rotation - screenOrientation;
        LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);


        int targetW, targetH;
        if (sensorOrientation == 90 || sensorOrientation == 270) {
            targetH = previewWidth;
            targetW = previewHeight;
        }
        else {
            targetW = previewWidth;
            targetH = previewHeight;
        }
        int cropW = (int) (targetW / 2.0);
        int cropH = (int) (targetH / 2.0);

        croppedBitmap = Bitmap.createBitmap(cropW, cropH, Bitmap.Config.ARGB_8888);

        portraitBmp = Bitmap.createBitmap(targetW, targetH, Bitmap.Config.ARGB_8888);
        faceBmp = Bitmap.createBitmap(TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE, Bitmap.Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropW, cropH,
                        sensorOrientation, MAINTAIN_ASPECT);

//    frameToCropTransform =
//            ImageUtils.getTransformationMatrix(
//                    previewWidth, previewHeight,
//                    previewWidth, previewHeight,
//                    sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);



        trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);
        trackingOverlay.addCallback(
                new OverlayView.DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);
                        if (isDebug()) {
                            tracker.drawDebug(canvas);
                        }
                    }
                });

        tracker.setFrameConfiguration(previewWidth, previewHeight, sensorOrientation);
    }


    @Override
    protected void processImage() {
        ++timestamp;
        final long currTimestamp = timestamp;
        trackingOverlay.postInvalidate();

        // No mutex needed as this method is not reentrant.
        if (computingDetection) {
            readyForNextImage();
            return;
        }
        computingDetection = true;
        LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");

        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

        readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
        // For examining the actual TF input.
        if (SAVE_PREVIEW_BITMAP) {
            ImageUtils.saveBitmap(croppedBitmap);
        }

        InputImage image = InputImage.fromBitmap(croppedBitmap, 0);
        faceDetector.process(image)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<List<Face>>() {
                    @Override
                    public void onSuccess(final List<Face> faces) {
                        if (faces.size() == 0) {
                            updateResults(currTimestamp, new LinkedList<Classifier.Recognition>());
                            return;
                        }
                        new Handler(getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                onFacesDetected(currTimestamp, faces);
                            }
                        });
                    }
                });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.tfe_od_camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RECORD_REQUEST_CODE && resultCode == RESULT_OK) {
            mediaProjection = projectionManager.getMediaProjection(resultCode, data);
            recordService.setMediaProject(mediaProjection);
            recordService.initImageReader();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        mIsNewTask = true;
        mMaskResultArr.clear();
        mProgressLayout.setVisibility(View.GONE);
    }

    private void checkFile(File dir, String Language){
        //디렉토리가 없으면 디렉토리를 만들고 그후에 파일을 카피
        if (!dir.exists() && dir.mkdirs()) {
            copyFiles(Language);
        }
        //디렉토리가 있지만 파일이 없으면 파일카피 진행
        if (dir.exists()) {
            String datafilepath = mDataPath + "tessdata/" + Language + ".traineddata";
            File datafile = new File(datafilepath);
            if (!datafile.exists()) {
                copyFiles(Language);
            }
        }
    }

    //copy file to device
    private void copyFiles(String Language) {
        try {
            String filepath = mDataPath + "/tessdata/" + Language + ".traineddata";
            AssetManager assetManager = getAssets();
            InputStream instream = assetManager.open("tessdata/"+Language+".traineddata");
            OutputStream outstream = new FileOutputStream(filepath);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }
            outstream.flush();
            outstream.close();
            instream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processImage(final Bitmap bitmap) {
        if(bitmap == null){
            Log.d("Timer", "NULL");
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tess.setImage(bitmap);

                final String OCRResult = tess.getUTF8Text();

                if (OCRResult.contains("인중되었습니다") && !mIsProcessing || OCRResult.contains("인증되었습니다") && !mIsProcessing || OCRResult.contains("인종되었") && !mIsProcessing ||
                        OCRResult.contains("등록된") && !mIsProcessing || OCRResult.contains("등록됨") && !mIsProcessing || OCRResult.contains("드록되") && !mIsProcessing
                        || OCRResult.contains("이미") && !mIsProcessing) {
                    mIsProcessing = true;

                    if (Build.VERSION.SDK_INT >= 11) {
                        ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
                        List<ActivityManager.AppTask> tasks = am.getAppTasks();

                        for (ActivityManager.AppTask task : tasks) {
                            if(task.getTaskInfo() == null || task.getTaskInfo().baseActivity == null) continue;
                            Log.d("TAG", "stackId: " + task.getTaskInfo().id);
                            if (task.getTaskInfo().baseActivity.toShortString().indexOf("DetectorActivity") > -1) {
                                am.moveTaskToFront(task.getTaskInfo().id, ActivityManager.MOVE_TASK_WITH_HOME);
                            }
                        }
                    }

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mIsProcessing = false;
                        }
                    }, 3000);
                }

                Log.d("Timer OCR", OCRResult);
            }
        });
    }

    private void initBroadCastReceiver(){
        IntentFilter arduinoFilter = new IntentFilter();
        arduinoFilter.addAction(GatePassWay.BROAD_CAST_ARDUINO_MESSAGE);
        registerReceiver(mArduninoReceiver, arduinoFilter);

        IntentFilter screenCaptureFilter = new IntentFilter();
        screenCaptureFilter.addAction(GatePassWay.BROAD_CAST_CAPTURE_UPDATE_MESSAGE);
        registerReceiver(mCaptureReceiver, screenCaptureFilter);

        IntentFilter arduinoStatusFilter = new IntentFilter();
        arduinoStatusFilter.addAction(GatePassWay.BROAD_CAST_ARDUINO_STATUS_MESSAGE);
        registerReceiver(mArduninoStatusReceiver, arduinoStatusFilter);
    }

    // Which detection model to use: by default uses Tensorflow Object Detection API frozen
    // checkpoints.
    private enum DetectorMode {
        TF_OD_API;
    }

    @Override
    protected void setUseNNAPI(final boolean isChecked) {
        runInBackground(new Runnable() {
            @Override
            public void run() {
                detector.setUseNNAPI(isChecked);
            }
        });
    }

    @Override
    protected void setNumThreads(final int numThreads) {
        runInBackground(new Runnable() {
            @Override
            public void run() {
                detector.setNumThreads(numThreads);
            }
        });
    }


    private void makeTTS(final String msg){
        try {
            mTTS.speak(msg, TextToSpeech.QUEUE_ADD, null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initTTS() {
        if (mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
            mTTS = null;
        }

        mTTS = new TextToSpeech(DetectorActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    //mTTS.setEngineByPackageName("com.google.android.tts");
                    //구글 API langCode는 ja인데 ja_JP를 넣어야 음성이 나온다.
                    Locale loc = new Locale("ko");
                    mTTS.setLanguage(loc);

                    mTTS.setPitch(1.0f);
                    mTTS.setSpeechRate(1.0f);
                }
            }
        });
    }

    // Face Mask Processing
    private Matrix createTransform(
            final int srcWidth,
            final int srcHeight,
            final int dstWidth,
            final int dstHeight,
            final int applyRotation) {

        Matrix matrix = new Matrix();
        if (applyRotation != 0) {
            if (applyRotation % 90 != 0) {
                LOGGER.w("Rotation of %d % 90 != 0", applyRotation);
            }

            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f);

            // Rotate around origin.
            matrix.postRotate(applyRotation);
        }

//        // Account for the already applied rotation, if any, and then determine how
//        // much scaling is needed for each axis.
//        final boolean transpose = (Math.abs(applyRotation) + 90) % 180 == 0;
//
//        final int inWidth = transpose ? srcHeight : srcWidth;
//        final int inHeight = transpose ? srcWidth : srcHeight;

        if (applyRotation != 0) {

            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f);
        }

        return matrix;

    }

    private void updateResults(long currTimestamp, final List<Classifier.Recognition> mappedRecognitions) {

        tracker.trackResults(mappedRecognitions, currTimestamp);
        trackingOverlay.postInvalidate();
        computingDetection = false;


        runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        showFrameInfo(previewWidth + "x" + previewHeight);
                        showCropInfo(croppedBitmap.getWidth() + "x" + croppedBitmap.getHeight());
                        showInference(lastProcessingTimeMs + "ms");
                    }
                });

    }

    private void onFacesDetected(long currTimestamp, List<Face> faces) {

        cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
        final Canvas canvas = new Canvas(cropCopyBitmap);
        final Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);

        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
        switch (MODE) {
            case TF_OD_API:
                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                break;
        }

        final List<Classifier.Recognition> mappedRecognitions =
                new LinkedList<Classifier.Recognition>();


        //final List<Classifier.Recognition> results = new ArrayList<>();

        // Note this can be done only once
        int sourceW = rgbFrameBitmap.getWidth();
        int sourceH = rgbFrameBitmap.getHeight();
        int targetW = portraitBmp.getWidth();
        int targetH = portraitBmp.getHeight();
        Matrix transform = createTransform(
                sourceW,
                sourceH,
                targetW,
                targetH,
                sensorOrientation);
        final Canvas cv = new Canvas(portraitBmp);

        // draws the original image in portrait mode.
        cv.drawBitmap(rgbFrameBitmap, transform, null);

        final Canvas cvFace = new Canvas(faceBmp);

        boolean saved = false;

        for (Face face : faces) {

            LOGGER.i("FACE" + face.toString());
            LOGGER.i("Running detection on face " + currTimestamp);

            //results = detector.recognizeImage(croppedBitmap);


            final RectF boundingBox = new RectF(face.getBoundingBox());

            //final boolean goodConfidence = result.getConfidence() >= minimumConfidence;
            final boolean goodConfidence = true; //face.get;
            if (boundingBox != null && goodConfidence) {

                // maps crop coordinates to original
                cropToFrameTransform.mapRect(boundingBox);

                // maps original coordinates to portrait coordinates
                RectF faceBB = new RectF(boundingBox);
                transform.mapRect(faceBB);

                // translates portrait to origin and scales to fit input inference size
                //cv.drawRect(faceBB, paint);
                float sx = ((float) TF_OD_API_INPUT_SIZE) / faceBB.width();
                float sy = ((float) TF_OD_API_INPUT_SIZE) / faceBB.height();
                Matrix matrix = new Matrix();
                matrix.postTranslate(-faceBB.left, -faceBB.top);
                matrix.postScale(sx, sy);

                cvFace.drawBitmap(portraitBmp, matrix, null);


                String label = "";
                float confidence = -1f;
                Integer color = Color.BLUE;
                Integer resultColor = 0;

                final long startTime = SystemClock.uptimeMillis();
                final List<Classifier.Recognition> resultsAux = detector.recognizeImage(faceBmp);
                lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                if(Preferences.getDetectNose()) {
                    Mat mat = new Mat();
                    MatOfRect faceDetections = new MatOfRect();

                    mIsNose = false;

                    Utils.bitmapToMat(faceBmp, mat);
                    faceDetectorCV.detectMultiScale(mat, faceDetections);
                    mIsNose = faceDetections.toArray().length > 0;

                    for (Rect rect : faceDetections.toArray()) {
                        Imgproc.rectangle(mat, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0));
                    }

                    Bitmap image1 = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888); // 비트맵 생성
                    Utils.matToBitmap(mat, image1); // Mat을 비트맵으로 변환
                    if(mMaskResultArr.size() < mMaskResltFrame) {
                        cropImg.setImageBitmap(image1);
                    }
                }else{
                    mIsNose = false;
                    if(mMaskResultArr.size() < mMaskResltFrame) {
                        cropImg.setImageBitmap(faceBmp);
                    }
                }



                if (resultsAux.size() > 0) {

                    Classifier.Recognition result = resultsAux.get(0);

                    float conf = result.getConfidence();
                    if (conf >= 0.6f) {
                        confidence = conf;
                        label = result.getTitle();
                        if (result.getId().equals("0")) {
                            if(mIsNose) {
                                label = "Incorrect";
                                color = Color.YELLOW;
                                resultColor = 1;
                            }
                            else {
                                color = Color.GREEN;
                                resultColor = 0;
                            }
                        }
                        else {
                            color = Color.RED;
                            resultColor = 2;
                        }

                        mMaskResultArr.add(resultColor);
                        if(mIsNewTask && mMaskResultArr.size() >= mMaskResltFrame){
                            gateOpenTask(isMaskCorrent());
                            mIsNewTask = false;
                            mMaskResultArr.clear();
                        }
                    }

                }

                if (getCameraFacing() == CameraCharacteristics.LENS_FACING_FRONT) {

                    // camera is frontal so the image is flipped horizontally
                    // flips horizontally
                    Matrix flip = new Matrix();
                    if (sensorOrientation == 90 || sensorOrientation == 270) {
                        flip.postScale(1, -1, previewWidth / 2.0f, previewHeight / 2.0f);
                    }
                    else {
                        flip.postScale(-1, 1, previewWidth / 2.0f, previewHeight / 2.0f);
                    }
                    //flip.postScale(1, -1, targetW / 2.0f, targetH / 2.0f);
                    flip.mapRect(boundingBox);

                }

                final Classifier.Recognition result = new Classifier.Recognition(
                        "0", label, confidence, boundingBox);

                result.setColor(color);
                result.setLocation(boundingBox);
                mappedRecognitions.add(result);

            }


        }

        //    if (saved) {
//      lastSaved = System.currentTimeMillis();
//    }

        updateResults(currTimestamp, mappedRecognitions);
    }

    private Integer isMaskCorrent(){
        int result[] = new int[3];
        result[0] = result[1] = result[2] = 0;
        for(Integer value : mMaskResultArr){
            result[value]++;
        }

        int maxIdx = 0;
        for(int i = 1; i < 3; ++i){
            if(result[maxIdx] < result[i]) maxIdx = i;
        }

        return maxIdx;
    }

    private void startTimerTask()
    {
        //showShortMsg("startTimerTask");
        //stopTimerTask();

        timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int per = 100 / mMaxTimeCount;
                        //mProgressBar.setProgressWithAnimation(100 - (per * mTimeCount), Long.parseLong("100")); // =1.5s
                        mProgressBar.setProgress(100 - (per * mTimeCount)); // =1.5s
                        mTvTimeCount.setText(Integer.toString(mMaxTimeCount-mTimeCount));
                        ++mTimeCount;
                        if(mTimeCount > mMaxTimeCount){
                            mTimeCount = 0;
                            stopTimerTask();
                            mTvTimeCount.setText("0");
                            mProgressBar.setProgress(0);

                            new Handler(getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mTvTimeCount.setText(Integer.toString(mMaxTimeCount));
                                    mProgressBar.setProgress(100);
                                    mProgressLayout.setVisibility(View.GONE);
                                }
                            }, 2000);

                        }
                    }
                });

            }
        };
        timer.schedule(timerTask,0 ,1000);
    }

    public Bitmap cropBitmap(Bitmap bitmap, int width, int height) {
        int originWidth = bitmap.getWidth();
        int originHeight = bitmap.getHeight();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenHeight = metrics.heightPixels;

        // 이미지를 crop 할 좌상단 좌표
        int x = 200;
        int y = 100;
        Bitmap cropedBitmap = Bitmap.createBitmap(bitmap, x, y, width - 400, height);


        return cropedBitmap;
    }

    private void stopTimerTask()
    {
        if(timerTask != null)
        {
            Log.d("Timer", "stopTimerTask");
            timerTask.cancel();
            timerTask = null;
        }

    }

    private void gateOpenTask(Integer color){
        if(color == 2){ //마스크 미착용
            makeTTS("마스크를 착용해 주세요.");
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mIsNewTask = true;
                }
            }, 3000);

        }else if(color == 0){ //마스크 착용
            makeTTS("게이트가 열립니다. 4초 안에 진입해 주세요.");
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendBroadcast(new Intent(GatePassWay.BROAD_CAST_SERIAL_SEND_MESSAGE));
                    mProgressLayout.setVisibility(View.VISIBLE);
                    startTimerTask();
                }
            }, 3000);

            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent it = getPackageManager().getLaunchIntentForPackage("com.jinsit.safepass");
                    it.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(it);
                    cropImg.setImageBitmap(null);
                }
            }, 9000);
        }else if(color == 1){ //마스크 올바르게 착용 안함
            makeTTS("마스크를 올바르게 착용해 주세요.");
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mIsNewTask = true;
                }
            }, 3000);
        }
    }

    private BroadcastReceiver mCaptureReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null) {
                processImage(GatePassWay.captureBmp);
            }
        }
    };

    private BroadcastReceiver mArduninoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null) {
                Toast.makeText(DetectorActivity.this, "근접", Toast.LENGTH_SHORT).show();
                Intent it = getPackageManager().getLaunchIntentForPackage("com.jinsit.safepass");
                it.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(it);
            }
        }
    };

    private BroadcastReceiver mArduninoStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null) {
                boolean status = intent.getBooleanExtra("status", false);
                if(status){
                    mIvSensorStatus.setVisibility(View.GONE);
                }else{
                    mIvSensorStatus.setVisibility(View.VISIBLE);
                }
            }
        }
    };

}
