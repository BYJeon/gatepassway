package com.waveon.gatepassway.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.waveon.gatepassway.R;
import com.waveon.gatepassway.activities.base.BindActivity;
import com.waveon.gatepassway.databinding.ActivitySplashBinding;
import com.waveon.gatepassway.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends BindActivity<ActivitySplashBinding> {
    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.REORDER_TASKS
    };
    private static final int REQUEST_CODE = 1;
    private List<String> mMissPermissions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Preferences.init(this);

        //모든 오류를 잡자
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                String stackTraceMsg = "paramThread: " + paramThread + " paramThrowable: " + paramThrowable + "\n";
                StackTraceElement[] stackTraces = paramThrowable.getStackTrace();

                for (int i = 0; i < stackTraces.length; i++) {
                    stackTraceMsg += stackTraces[i].toString();
                    stackTraceMsg += "\n";
                }
                paramThrowable = paramThrowable.getCause();
                if (paramThrowable != null) {
                    stackTraceMsg += "Caused by: " + paramThrowable + "\n";
                    stackTraces = paramThrowable.getStackTrace();
                    for (int i = 0; i < stackTraces.length; i++) {
                        stackTraceMsg += stackTraces[i].toString();
                        stackTraceMsg += "\n";
                    }

                    paramThrowable = paramThrowable.getCause();
                    if (paramThrowable != null) {
                        stackTraceMsg += "Caused by: " + paramThrowable + "\n";
                        stackTraces = paramThrowable.getStackTrace();
                        for (int i = 0; i < stackTraces.length; i++) {
                            stackTraceMsg += stackTraces[i].toString();
                            stackTraceMsg += "\n";
                        }
                    }
                }

                Log.e("Crash", stackTraceMsg);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        if (isVersionM()) {
            checkAndRequestPermissions();
        } else {
            startMainActivity();
        }
    }

    @Override
    protected void setUIEventListener() {

    }

    private boolean isVersionM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    private void checkAndRequestPermissions() {
        mMissPermissions.clear();
        for (String permission : REQUIRED_PERMISSION_LIST) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                mMissPermissions.add(permission);
            }
        }
        // check permissions has granted
        if (mMissPermissions.isEmpty()) {
            startMainActivity();
        } else {
            ActivityCompat.requestPermissions(this,
                    mMissPermissions.toArray(new String[mMissPermissions.size()]),
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            for (int i = grantResults.length - 1; i >= 0; i--) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    mMissPermissions.remove(permissions[i]);
                }
            }
        }
        // Get permissions success or not
        if (mMissPermissions.isEmpty()) {
            startMainActivity();
        } else {
            Toast.makeText(SplashActivity.this, "권한을 허용해 주세요!", Toast.LENGTH_SHORT).show();
            SplashActivity.this.finish();
        }
    }

    private void startMainActivity() {
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, DetectorActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
